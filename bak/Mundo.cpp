// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#define SPEED 2+2*rand()/(float)RAND_MAX;
#define SPEEDI 2
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);

}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{

	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	ammus.Dibuja();
	//		AQUI TERMINA MI DIBUJO

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{

if (ammus.osuij2==1) {
    if (ammus.ajastinj1 <= 100) {
        jugador1.Mueve(0.0f); //the timer for the freeze j1
        ammus.ajastinj1++;
    }
    else {ammus.osuij1 = 0;}}
else { jugador1.Mueve(0.025f); }

if (ammus.osuij1==1) {
    if (ammus.ajastinj2 <= 100) {
        jugador2.Mueve(0.0f); //the timer for the freeze of j2
        ammus.ajastinj2++;
    }
    else {ammus.osuij1 = 0;}}
 else { jugador2.Mueve(0.025f);}

esfera.Mueve(0.025f);
if (a==1) {

ammus.Mueve(0.025f);
 }
 else {ammus.centro.x=-255;}

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		if (a==1) {paredes[i].Rebota(ammus); }

		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	//ammus

	if (a==1) {
    jugador1.Rebota(ammus);
	jugador2.Rebota(ammus);

	//j1 begin
	if((fondo_izq.Rebota(ammus))==true)
	{
	ammus.centro.x=-255;
	ammus.velocidad.x=0;
	//a=0;
printf("MISSED\n");
    }

    if  ((((jugador1.x1 + jugador1.x2) / 2) <= ammus.centro.x) && ((((jugador1.y1 + jugador1.y2) /2 )+0.5) >= ammus.centro.y) && ((((jugador1.y1 + jugador1.y2) /2 )-0.5) <= ammus.centro.y)) {

    printf("HIT\n");
    ammus.centro.x=255;
    ammus.centro.y=255;
	ammus.velocidad.x=0;
	ammus.osuij2=1;
	ammus.ajastinj1=0;
        //a=0;
    }

	//j2 begin
	if((fondo_dcho.Rebota(ammus))==true)
	{
	ammus.centro.x=-255;
	ammus.velocidad.x=0;
	a=0;
printf("MISSED\n");
    }

    if  ((((jugador2.x1 + jugador2.x2) / 2) <= ammus.centro.x) && ((((jugador2.y1 + jugador2.y2) /2 )+0.5) >= ammus.centro.y) && ((((jugador2.y1 + jugador2.y2) /2 )-0.5) <= ammus.centro.y)) {

    printf("HIT\n");
    ammus.centro.x=255;
    ammus.centro.y=255;
	ammus.velocidad.x=0;
	ammus.osuij1=1;
	ammus.ajastinj2=0;
        //a=0;
    }

//float j2xc = ((jugador2.x1 + jugador2.x2) / 2);
//float jyc = ((jugador2.y1 + jugador2.y2) / 2);

 // printf("debug: jyc %i ammus centro %i \n", ammus.osuij1,ammus.ajastin);//")jugador1.x1==ammus.centro.x ) {
  //  usleep(10*1000);
}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=SPEEDI;
		esfera.velocidad.y=SPEEDI;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=SPEEDI;
		esfera.velocidad.y=SPEEDI;
		puntos1++;
        ammus.centro.x=0;
        ammus.centro.y=0;
        ammus.velocidad.x=3;
        ammus.velocidad.y=0;
    a=1;
	}

	// -------------------------------------
	// --------------------------------------- //
	// ---------- COMPLETAR AQUÍ ------------- //
	// --------------------------------------- //
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
        // case 'space' if (pallo hallussa ) ammus.mueve;
	}
}

void CMundo::Init()
{
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p); // se añade la pared inferior al vector paredes

	//pared superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;//jugador1.y1=-1;
	jugador1.x2=-6;//jugador1.y2=1;
//para 1.15 disminue el raqueta
	if(puntos1<=1) {
	jugador1.y1=-1;
	jugador1.y2=1;
}
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;
	jugador2.x2=6;
	if(puntos2<=1) {
	jugador2.y1=-1;
	jugador2.y2=1;
}

}
