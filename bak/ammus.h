
// Ammus.h: interface for the Ammus class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_Ammus_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_Ammus_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Ammus
{
public:
	Ammus();
	virtual ~Ammus();

	Vector2D centro;
	Vector2D velocidad;
	float radio;
	int ajastinj1;
	int ajastinj2;
	int osuij1;
	int osuij2;
	void Mueve(float t);
	void Dibuja();
};

#endif // !defined(AFX_Ammus_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
