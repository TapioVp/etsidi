
// Ammus.cpp: implementation of the Ammus class.
//
//////////////////////////////////////////////////////////////////////
//#include <X11/Xlib.h>
// #include <GL/glut.h>
//#include <stdio.h>
//#include <stdlib.h>
//#ifdef _WIN32
//#	include <windows.h>
//#else
//#	include <sys/time.h>
#include "ammus.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Ammus::Ammus()
{
	radio=0.2f;
	velocidad.x=4;
	velocidad.y=4;
    ajastinj1=111;
    ajastinj2=111;
    osuij1 = 0;
    osuij2 = 0;

}

Ammus::~Ammus()
{

}



void Ammus::Dibuja()
{
	glColor3ub(155,155,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Ammus::Mueve(float t)
{
	centro=centro+(velocidad*t);
}

