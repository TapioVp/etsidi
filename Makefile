CC=g++
CFLAGS =-g -Wall -fexceptions -g
LDFLAGS =  -lGL -lGLU -lglut -lm 
OBJS = ammus.o Esfera.o Mundo.o Plano.o Raqueta.o tenis.o Vector2D.o

all: softa

ammus.o: ammus.cpp ammus.h
	g++ -Wall -fexceptions -g  -c ammus.cpp -o ammus.o
Esfera.o: Esfera.cpp Esfera.h
	g++ -Wall -fexceptions -g  -c Esfera.cpp -o Esfera.o
Mundo.o: Mundo.cpp Mundo.h
	g++ -Wall -fexceptions -g -c Mundo.cpp -o Mundo.o
Plano.o: Plano.cpp Plano.h
	g++ -Wall -fexceptions -g  -c Plano.cpp -o Plano.o
Raqueta.o: Raqueta.cpp Raqueta.h
	g++ -Wall -fexceptions -g  -c Raqueta.cpp -o Raqueta.o
tenis.o: tenis.cpp
	g++ -Wall -fexceptions -g  -c tenis.cpp -o tenis.o
Vector2D.o: Vector2D.cpp Vector2D.h
	g++ -Wall -fexceptions -g  -c Vector2D.cpp -o Vector2D.o

softa: $(OBJS)
	 $(CC) ${OBJS} -o programa $(CFLAGS) ${LDFLAGS} 

clean:
	rm -rf *o programa
