// Mundo.cpp: implementation of the CMundo class.
// Added functions:
//  gameplay:
//      moving of raquetas and tennis ball,
//      an penaltyshot for the opponent when you miss the ball:
//          when hitting the player will either freeze the player for a while or diminish
//          the player's raquetas size.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"
#include "ammus.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#define SPEEDI 2+2*rand()/(float)RAND_MAX;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);

}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{

	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	if(ammusdejugador1.ajastin <= 100) {  sprintf(cad,"FREEZE TIMER J2: %i",100-ammusdejugador1.ajastin);	print(cad,400,0,1,1,1); }
	if(ammusdejugador2.ajastin <= 100) {  sprintf(cad,"FREEZE TIMER J1: %i",100-ammusdejugador2.ajastin);	print(cad,200,0,1,1,1); }
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
    esfera.Dibuja();
	ammusdejugador1.Dibuja();
    ammusdejugador2.Dibuja();
	//		AQUI TERMINA MI DIBUJO

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{


if (ammusdejugador2.osui==1) {
    if (ammusdejugador2.ajastin <= 100) {
        jugador1.Mueve(0.0f);
        ammusdejugador2.ajastin++;
    }
    else {ammusdejugador2.osui = 0;}
}
 else {
jugador1.Mueve(0.025f);
}

if (ammusdejugador1.osui==1) {
    if (ammusdejugador1.ajastin <= 100) {
        jugador2.Mueve(0.0f);
        ammusdejugador1.ajastin++;
    }
    else {ammusdejugador1.osui = 0;}
}
 else {
jugador2.Mueve(0.025f);
}
esfera.Mueve(0.025f);
if (a==1) {
ammusdejugador1.Mueve(0.025f);
if (a_stay==1) {
ammusdejugador1.centro.x=jugador1.x1+0.25;
ammusdejugador1.centro.y=(jugador1.y1+jugador1.y2)/2;
}
 }
 if (b==1) {
ammusdejugador2.Mueve(-0.025f);
if (b_stay==1) {
ammusdejugador2.centro.x=jugador2.x1-0.25;
ammusdejugador2.centro.y=(jugador2.y1+jugador2.y2)/2;
}
 }

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		if (a==1) {paredes[i].Rebota(ammusdejugador1); }
		if (b==1) {paredes[i].Rebota(ammusdejugador2); }


		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

if (a==1) {
       jugador2.Rebota(ammusdejugador1);
    if((fondo_dcho.Rebota(ammusdejugador1))==true)
        {
            ammusdejugador1.centro.x=-255;
            ammusdejugador1.velocidad.x=0;
            a=0;
            printf("J1 MISSED\n");
        }

    if  (((((jugador2.x1 + jugador2.x2) / 2)-0.5) <= ammusdejugador1.centro.x) && ((((jugador2.y1 + jugador2.y2) /2 )+0.5) >= ammusdejugador1.centro.y) && ((((jugador2.y1 + jugador2.y2) /2 )-0.5) <= ammusdejugador1.centro.y)) {
        printf("J1 HIT\n");
        ammusdejugador1.centro.x=255;
        ammusdejugador1.velocidad.x=0;
        a=0;
        if (rand()%2==1) {
        ammusdejugador1.osui=1;
        ammusdejugador1.ajastin=0;
        } else {
        if (jugador2.pienennys<= 0.5) { jugador2.pienennys += 0.1;} }
     }
}
if (b==1) {
    jugador1.Rebota(ammusdejugador2);
    if((fondo_izq.Rebota(ammusdejugador2))==true)
        {
            ammusdejugador2.centro.x=-255;
            ammusdejugador2.velocidad.x=0;
            b=0;
            printf("j2 MISSED\n");
        }

    if  (((((jugador1.x1 + jugador1.x2) / 2)+0.5) == ammusdejugador2.centro.x) && ((((jugador1.y1 + jugador1.y2) /2 )+0.5) >= ammusdejugador2.centro.y) && ((((jugador1.y1 + jugador1.y2) /2 )-0.5) <= ammusdejugador2.centro.y)) {
        printf("j2 HIT\n");
        ammusdejugador2.centro.x=255;
        ammusdejugador2.velocidad.x=0;
        b=0;
        if (rand()%2==1) {
        ammusdejugador2.osui=1;
        ammusdejugador2.ajastin=0;
        }
        else {
        if (jugador1.pienennys<= 0.5) { jugador1.pienennys += 0.1;} }
     }
}
	if(fondo_izq.Rebota(esfera))

	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=SPEEDI;
		esfera.velocidad.y=SPEEDI;
		puntos2++;

        ammusdejugador2.centro.x=jugador2.x1-0.5;
        ammusdejugador2.centro.y=(jugador2.y1+jugador2.y2)/2;
        ammusdejugador2.velocidad.x=0;
        ammusdejugador2.velocidad.y=0;
        b=1;
        b_stay=1;
        ammusdejugador2.pallohallussa=1;

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=SPEEDI;
		esfera.velocidad.y=SPEEDI;
		puntos1++;
        ammusdejugador1.centro.x=jugador1.x1+0.25;
        ammusdejugador1.centro.y=(jugador1.y1+jugador1.y2)/2;
        ammusdejugador1.velocidad.x=0;
        ammusdejugador1.velocidad.y=0;
        a=1;
        a_stay=1;
        ammusdejugador1.pallohallussa=1;
	}

	// -------------------------------------
	// --------------------------------------- //
	// ---------- COMPLETAR AQUÍ ------------- //
	// --------------------------------------- //
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
        case 'f': // left player shoot the bullet
        if(ammusdejugador1.pallohallussa==1) {
        ammusdejugador1.centro.x=jugador1.x1+0.25;
        ammusdejugador1.centro.y=(jugador1.y1+jugador1.y2)/2;
        ammusdejugador1.velocidad.x=5;
        ammusdejugador1.velocidad.y=0;
        a=1;
        a_stay=0;
        ammusdejugador1.pallohallussa=0;
        }
        break;
        case 'j': //right player shoot the bullet
        if(ammusdejugador2.pallohallussa==1) {
        ammusdejugador2.centro.x=jugador2.x1-0.5;
        ammusdejugador2.centro.y=(jugador2.y1+jugador2.y2)/2;
        ammusdejugador2.velocidad.x=5;
        ammusdejugador2.velocidad.y=0;
        b=1;
        b_stay=0;
        ammusdejugador2.pallohallussa=0;
        }
        break;

	}
}

void CMundo::Init()
{
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p); // se añade la pared inferior al vector paredes

	//pared superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;//jugador1.y1=-1;
	jugador1.x2=-6;//jugador1.y2=1;
	jugador1.y1=-1;
	jugador1.y2=1;
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;
	jugador2.x2=6;
	if(puntos2<=1) {
	jugador2.y1=-1;
	jugador2.y2=1;
}

}
