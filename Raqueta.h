// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_)
#define AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano
{
public:
	void Mueve(float t);
	Raqueta();
	virtual ~Raqueta();

	Vector2D velocidad;

};

#endif // !defined(AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_)
