// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#define SPEED 2+2*rand()/(float)RAND_MAX;
#define SPEEDI 2
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);

}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{

	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	ammus.Dibuja();
	//		AQUI TERMINA MI DIBUJO

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
int osuij1;
int ohimenitimer;

jugador1.Mueve(0.025f);
if (osuij1==1) {
    if (ohimenitimer <= 100) {
        jugador2.Mueve(0.0f);
        ohimenitimer++;
    }
osuij1 = 0;
}

 else {
jugador2.Mueve(0.025f);
}
esfera.Mueve(0.025f);
if (a==1) {

ammus.Mueve(0.025f);
 }

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		if (a==1) {paredes[i].Rebota(ammus); }

		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	//ammus

	if (a==1) {
    jugador1.Rebota(ammus);
	jugador2.Rebota(ammus);
	if((fondo_dcho.Rebota(ammus))==true)
	{
	ammus.centro.x=-255;
	ammus.velocidad.x=0;
	a=0;
printf("MISSED\n");
    }

    if  ((fondo_dcho.Rebota(ammus) ==false)  && (jugador2.x1 <= 2) && (jugador2.x1 >= -0.8)) {

    printf("HIT\n");
    ammus.centro.x=255;
	ammus.velocidad.x=0;
	osuij1=1;
        //a=0;
            }
    //printf("debug: jugador2.y1 %f ammus centro %f \n", jugador2.y1, ammus.centro.x);//")jugador1.x1==ammus.centro.x ) {
  //  usleep(10*1000);
}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=SPEEDI;
		esfera.velocidad.y=SPEEDI;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=SPEEDI;
		esfera.velocidad.y=SPEEDI;
		puntos1++;
        ammus.centro.x=0;
        ammus.centro.y=0;
        ammus.velocidad.x=3;
        ammus.velocidad.y=0;
    a=1;
	}

	// -------------------------------------
	// --------------------------------------- //
	// ---------- COMPLETAR AQUÍ ------------- //
	// --------------------------------------- //
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
        // case 'space' if (pallo hallussa ) ammus.mueve;
	}
}
b
void CMundo::Init()
{
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p); // se añade la pared inferior al vector paredes

	//pared superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;//jugador1.y1=-1;
	jugador1.x2=-6;//jugador1.y2=1;
//para 1.15 disminue el raqueta
	if(puntos1<=1) {
	jugador1.y1=-1;
	jugador1.y2=1;
}
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;
	jugador2.x2=6;
	if(puntos2<=1) {
	jugador2.y1=-1;
	jugador2.y2=1;
}

}
