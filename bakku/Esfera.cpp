// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////
//#include <X11/Xlib.h>
// #include <GL/glut.h>
//#include <stdio.h>
//#include <stdlib.h>
//#ifdef _WIN32
//#	include <windows.h>
//#else
//#	include <sys/time.h>
#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+(velocidad*t);
}
